// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// Generator: RxBlocGeneratorForAnnotation
// **************************************************************************

part of 'theme_bloc.dart';

/// ThemeBlocType class used for bloc event and state access from widgets
/// {@nodoc}
abstract class ThemeBlocType extends RxBlocTypeBase {
  ThemeBlocEvents get events;

  ThemeBlocStates get states;
}

/// $ThemeBloc class - extended by the ThemeBloc bloc
/// {@nodoc}
abstract class $ThemeBloc extends RxBlocBase
    implements ThemeBlocEvents, ThemeBlocStates, ThemeBlocType {
  ///region Events

  ///region themeChange

  final _$themeChangeEvent = PublishSubject<AppTheme>();
  @override
  void themeChange(AppTheme theme) => _$themeChangeEvent.add(theme);

  ///endregion themeChange

  ///endregion Events

  ///region States

  ///region themeItem
  Stream<AppTheme> _themeItemState;

  @override
  Stream<AppTheme> get themeItem => _themeItemState ??= _mapToThemeItemState();

  Stream<AppTheme> _mapToThemeItemState();

  ///endregion themeItem

  ///region changedEnabled
  Stream<bool> _changedEnabledState;

  @override
  Stream<bool> get changedEnabled =>
      _changedEnabledState ??= _mapToChangedEnabledState();

  Stream<bool> _mapToChangedEnabledState();

  ///endregion changedEnabled

  ///endregion States

  ///region Type

  @override
  ThemeBlocEvents get events => this;

  @override
  ThemeBlocStates get states => this;

  ///endregion Type

  /// Dispose of all the opened streams

  @override
  void dispose() {
    _$themeChangeEvent.close();
    super.dispose();
  }
}
