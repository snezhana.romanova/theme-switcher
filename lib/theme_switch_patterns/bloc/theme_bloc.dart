import 'package:rx_bloc/rx_bloc.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../utils/app_themes.dart';

part 'theme_bloc.rxb.g.dart';

abstract class ThemeBlocEvents {
  void themeChange(AppTheme theme);
}

abstract class ThemeBlocStates {
  Stream<AppTheme> get themeItem;
  Stream<bool> get changedEnabled;
}

final _themePreference = "theme_preference";

@RxBloc()
class ThemeBloc extends $ThemeBloc {
  final _themeItem = BehaviorSubject.seeded(
    AppTheme.values[0],
  );

  final _compositeSubscription = CompositeSubscription();

  ThemeBloc() {
    _$themeChangeEvent
        .map((themeItem) {
          SharedPreferences.getInstance().then((prefs) => prefs.setInt(
              _themePreference, AppTheme.values.indexOf(themeItem)));
          return themeItem;
        })
        .bind(_themeItem)
        .disposedBy(_compositeSubscription);
    SharedPreferences.getInstance().then(
      (prefs) {
        int preferredTheme = prefs.getInt(_themePreference) ?? 0;
        _themeItem.sink.add(AppTheme.values[preferredTheme]);
      },
    );
  }

  @override
  Stream<bool> _mapToChangedEnabledState() {
    return _themeItem.map((data) => data != null);
  }

  @override
  Stream<AppTheme> _mapToThemeItemState() {
    return _themeItem.map((value) => value);
  }

  @override
  void dispose() {
    _compositeSubscription.dispose();
    super.dispose();
  }
}
