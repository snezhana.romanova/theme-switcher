// // 1. Clear App
//
// import 'package:flutter/material.dart';
//
// import 'screens/flowers_overview_screen.dart';
// import 'utils/theme_switching_method.dart';
//
// void main() {
//   runApp(MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//           title: 'Theme switcher',
//           theme: ThemeData(),
//           home: FlowersOverviewScreen(themeSwitchingMethod: ThemeSwitchingMethod.None,),
//         );
//   }
// }


// // 2. Theme Switching using Provider
//
// import 'package:flutter/material.dart';
// import 'package:provider/provider.dart';
//
// import 'screens/flowers_overview_screen.dart';
// import 'utils/theme_switching_method.dart';
// import './theme_switch_patterns/provider/theme_manager.dart';
//
// void main() {
//   runApp(MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ChangeNotifierProvider(
//       create: (_) => ThemeManager(),
//       child: Consumer<ThemeManager>(
//         builder: (context, manager, _) => MaterialApp(
//           title: 'Theme switcher',
//           theme: manager.themeData,
//           home: FlowersOverviewScreen(
//             themeSwitchingMethod: ThemeSwitchingMethod.Provider,
//           ),
//         ),
//       ),
//     );
//   }
// }

//  // 3. Theme Switching using dynamic_theme
//
// import 'package:flutter/material.dart';
// import 'package:dynamic_theme/dynamic_theme.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// import 'screens/flowers_overview_screen.dart';
// import 'utils/theme_switching_method.dart';
// import 'utils/app_themes.dart';
//
// void main() {
//   runApp(MyApp());
// }
//
// class MyApp extends StatelessWidget {
//   Map<AppTheme, ThemeData> data = {};
//   final _kThemePreference = "theme_preference";
//
//   int dynTheme;
//   Future<void> getDefault() async {
//     data = appThemeData;
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     dynTheme = prefs.getInt(_kThemePreference) ?? 0;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     getDefault();
//     return DynamicTheme(
//         defaultBrightness: Brightness.light,
//         data:  (Brightness brightness) {
//           getDefault();
//           for(int i = 0; i < AppTheme.values.length ; i++){
//             if(i == dynTheme){
//               return appThemeData[AppTheme.values[i]];
//             }
//           }
//           return data[0];
//         },
//         themedWidgetBuilder: (context, theme) {
//           return new MaterialApp(
//             title: 'Flutter Demo',
//             theme: theme,
//             home: FlowersOverviewScreen(themeSwitchingMethod: ThemeSwitchingMethod.DynamicTheme,),
//           );
//         }
//     );
//   }
// }

// 4. Theme Switching using RxBLoC

import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:theme_swicher/theme_switch_patterns/bloc/theme_bloc.dart';
import 'package:theme_swicher/utils/app_themes.dart';

import 'screens/flowers_overview_screen.dart';
import 'utils/theme_switching_method.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RxBlocProvider<ThemeBlocType>(
      create:(_) => ThemeBloc(),
    child: RxBlocBuilder<ThemeBlocType, AppTheme>(
        state: (bloc ) => bloc.states.themeItem,
        builder: (context, state, _) {
      return MaterialApp(
          title: 'Theme switcher',
          theme: appThemeData[state.data],
          home: FlowersOverviewScreen(themeSwitchingMethod: ThemeSwitchingMethod.Bloc,),
        );
    },

    ),
    );
  }
}
