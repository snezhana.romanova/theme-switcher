import 'package:flutter/material.dart';

import '../screens/in_construction_screen.dart';
import '../screens/theme_switcher_screen.dart';
import '../utils/theme_switching_method.dart';

enum popUpValue { Orders, Cart, WishList, Account, Theme }

class SettingMenu extends StatelessWidget {
  final ThemeSwitchingMethod themeSwitchingMethod;

  const SettingMenu({Key key, @required this.themeSwitchingMethod})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: Icon(Icons.settings),
      itemBuilder: (_) => [
        PopupMenuItem(
          child: Text('My orders'),
          value: popUpValue.Orders,
        ),
        PopupMenuItem(
          child: Text('Shopping cart'),
          value: popUpValue.Cart,
        ),
        PopupMenuItem(
          child: Text('Wish List'),
          value: popUpValue.WishList,
        ),
        PopupMenuItem(
          child: Text('My account'),
          value: popUpValue.Account,
        ),
        PopupMenuItem(
          child: Text('Theme'),
          value: popUpValue.Theme,
        )
      ],
      onSelected: (selectedValue) {
        selectedValue == popUpValue.Theme
            ? Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ThemeSwitcherScreen(
                          themeSwitchingMethod: themeSwitchingMethod,
                        )))
            : Navigator.of(context).push<InConstructionScreen>(
                MaterialPageRoute(
                    builder: (context) => InConstructionScreen()));
      },
    );
  }
}
