import 'package:flutter/material.dart';

enum AppTheme { Pink, Dark, Cyan, DarkPurple }

/// Returns enum value name without enum class name.
String enumName(AppTheme anyEnum) {
  return anyEnum.toString().split('.')[1];
}

final appThemeData = {
  AppTheme.Pink: ThemeData(
      brightness: Brightness.light,
      primaryColor: Colors.pink,
      accentColor: Colors.greenAccent),
  AppTheme.Dark: ThemeData(
    brightness: Brightness.dark,
    primaryColor: Colors.black,
    accentColor: Colors.deepOrangeAccent,
  ),
  AppTheme.Cyan: ThemeData(
    brightness: Brightness.light,
    primaryColor: Colors.cyan,
    accentColor: Colors.pinkAccent,
  ),
  AppTheme.DarkPurple: ThemeData(
      brightness: Brightness.dark,
      primaryColor: Colors.deepPurple,
      accentColor: Colors.grey)
};
