import 'package:flutter/material.dart';

class Flower {
  final String name;
  final String photoUrl;
  final double price;
  final String description;

  Flower(
      {@required this.name,
      @required this.photoUrl,
      @required this.price,
      @required this.description});
}
