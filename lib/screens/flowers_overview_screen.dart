import 'package:flutter/material.dart';

import '../repositories/flower_repository.dart';
import '../utils/setting_menu.dart';
import '../utils/theme_switching_method.dart';
import '../widgets/flower_item.dart';

class FlowersOverviewScreen extends StatelessWidget {
  final ThemeSwitchingMethod themeSwitchingMethod;
  FlowersOverviewScreen({Key key, @required this.themeSwitchingMethod})
      : super(key: key);

  final FlowerRepository repository = FlowerRepository();

  @override
  Widget build(BuildContext context) {
    var flowers = repository.getFlowers();
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Flower\'s shop'),
          actions: [
            SettingMenu(
              themeSwitchingMethod: themeSwitchingMethod,
            ),
          ],
        ),
        body: ListView.builder(
          itemCount: flowers.length,
          itemBuilder: (context, i) => FlowerItem(
            flower: flowers[i],
          ),
        ));
  }
}
