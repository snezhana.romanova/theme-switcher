import 'package:flutter/material.dart';
import 'package:flutter_rx_bloc/flutter_rx_bloc.dart';
import 'package:provider/provider.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../theme_switch_patterns/bloc/theme_bloc.dart';
import '../theme_switch_patterns/provider/theme_manager.dart';
import '../utils/theme_switching_method.dart';
import '../utils/app_themes.dart';

class ThemeSwitcherScreen extends StatelessWidget {
  final ThemeSwitchingMethod themeSwitchingMethod;
  final _kThemePreference = "theme_preference";

  const ThemeSwitcherScreen({Key key, @required this.themeSwitchingMethod})
      : super(key: key);

  setSharedPrefs(AppTheme theme) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setInt(_kThemePreference, AppTheme.values.indexOf(theme));
  }

  @override
  Widget build(BuildContext context) {
    final themes = AppTheme.values;
    return Scaffold(
      appBar: AppBar(
        title: Text('Theme Switcher'),
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsets.all(10),
        child: ListView.builder(
          itemCount: themes.length,
          itemBuilder: (context, i) {
            final themeItem = themes[i];
            if (themeSwitchingMethod == ThemeSwitchingMethod.Bloc) {
              return RxBlocBuilder<ThemeBlocType, bool>(
                state: (bloc) => bloc.states.changedEnabled,
                builder: (context, snapshot, bloc) => Container(
                  height: 100,
                  width: MediaQuery.of(context).size.width * 0.9,
                  margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.grey),
                    borderRadius: BorderRadius.circular(15),
                    color: appThemeData[themeItem].primaryColor,
                  ),
                  alignment: Alignment.center,
                  child: ListTile(
                    title: Text(
                      enumName(themeItem),
                      style: appThemeData[themeItem].textTheme.bodyText1,
                    ),
                    onTap: () {
                      bloc.events.themeChange(themeItem);
                      Navigator.of(context).pop();
                    },
                  ),
                ),
              );
            } else {
              return Container(
                height: 100,
                width: MediaQuery.of(context).size.width * 0.9,
                margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.grey),
                  borderRadius: BorderRadius.circular(15),
                  color: appThemeData[themeItem].primaryColor,
                ),
                alignment: Alignment.center,
                child: ListTile(
                  title: Text(
                    enumName(themeItem),
                    style: appThemeData[themeItem].textTheme.bodyText1,
                  ),
                  onTap: () {
                    if (themeSwitchingMethod == ThemeSwitchingMethod.Provider) {
                      Provider.of<ThemeManager>(context, listen: false)
                          .setTheme(themeItem);
                    } else if (themeSwitchingMethod ==
                        ThemeSwitchingMethod.DynamicTheme) {
                      DynamicTheme.of(context)
                          .setThemeData(appThemeData[themeItem]);
                      setSharedPrefs(themeItem);
                    }
                    Navigator.of(context).pop();
                  },
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
