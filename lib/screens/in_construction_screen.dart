import 'package:flutter/material.dart';

class InConstructionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('In Construction'),
        leading: IconButton(
          icon: Icon(Icons.keyboard_backspace_rounded),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Center(
        child: Container(
          height: 250,
          width: 250,
          child: Image.asset('assets/images/coming-soon-page_550.jpg'),
        ),
      ),
    );
  }
}
