import '../models/flower_model.dart';

class FlowerRepository {
  final List<Flower> _flowers = [
    Flower(
        name: 'Rose Garden',
        description:
            'Bulgarian Red Roses, greens and babies breath In a glass vase',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/new_bg_03.jpg',
        price: 99.99),
    Flower(
        name: 'Classic Significance',
        description:
            'This Classic includes 12 colored Roses, an assortment of greens in a glass vase',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/0168-water.jpg',
        price: 67.99),
    Flower(
        name: 'Dramatic Effects Bouquet',
        description:
            'Bring on the drama with a swirl of rich red roses surrounded by clouds of white with this perfectly stunning fresh flower bouquet. ',
        photoUrl:
            'https://www.bulgariaflowers.com/images_new/ftd/C15-5176p.jpg',
        price: 99.00),
    Flower(
        name: 'Starshine Bouquet',
        description:
            'Hot pink bi-colored roses, orange Asiatic Lilies, green Fuji Chrysanthemums, hot pink mini carnations, tropical leaves, and lush greens are brought together to impress, presented in a clear glass vase.',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/ftd/D9-5211p.jpg',
        price: 87.00),
    Flower(
        name: 'The Epicurean Arrangement',
        description:
            'An arrangement featuring red roses and greens in a breathtaking glass vase',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/p1030695.jpg',
        price: 95.99),
    Flower(
        name: 'Nature\'s  Bounty Bouquet',
        description:
            'Bright, vibrant, and ready to inspire with each sun-kissed bloom, this simply stunning flower bouquet is a gift your recipient will always remember.',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/ftd/C3-5153p.jpg',
        price: 79.00),
    Flower(
        name: 'Contemporary Roses',
        description:
            'Fascinating at every turn, this modern and sophisticated rose bouquet is for the recipient that always knows what\'s on trend and loves being surrounded by nature\'s beauty.',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/ftd/E7-5241p.jpg',
        price: 139.00),
    Flower(
        name: 'Exotica Arrangement',
        description:
            'A departure from the everyday, this bouquet brings a touch of paradise into their lives with it\'s chic, modern styling and colorful blooms.',
        photoUrl:
            'https://www.bulgariaflowers.com/images_new/ftd/C21-5174p.jpg',
        price: 79.00),
    Flower(
        name: 'Candy Cane Basket',
        description:
            'A delightful presentation for Christmas. This item features stunning red Roses, Carnations, Greens, Alstroemeria and seasonal ornaments',
        photoUrl: 'https://www.bulgariaflowers.com/images_new/0220-water.jpg',
        price: 63.99),
    Flower(
        name: 'Pure Love',
        description:
            'Pure Love is a romantic presentation style piece for someone you love. Featuring 12 stunning Red Roses, White Orchids, Babys Breath, and tropical greens tied with a colored net and bow',
        photoUrl:
            'https://www.bulgariaflowers.com/images_new/val200910_300.jpg',
        price: 82.45),
    Flower(
        name: 'Basket of Dreams',
        description:
            'The FTD® Basket of Dreams™ blooms with vibrant color to capture their every attention. ',
        photoUrl:
            'https://www.bulgariaflowers.com/images_new/ftd/C15-4856p.jpg',
        price: 77.50),
  ];
  FlowerRepository();

  List<Flower> getFlowers() {
    return [..._flowers];
  }
}
