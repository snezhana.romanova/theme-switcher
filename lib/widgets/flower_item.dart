import 'dart:ui';
import 'package:flutter/material.dart';

import '../models/flower_model.dart';

class FlowerItem extends StatelessWidget {
  final Flower flower;

  const FlowerItem({Key key, this.flower}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: MediaQuery.of(context).size.width * 0.9,
      margin: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      decoration: BoxDecoration(
        border: Border.all(width: 1, color: Colors.grey),
        borderRadius: BorderRadius.circular(15),
        gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          colors: [
            Theme.of(context).primaryColor.withOpacity(0.8),
            Theme.of(context).primaryColor.withOpacity(0.3),
          ],
        ),
      ),
      alignment: Alignment.center,
      child: ListTile(
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Hero(
              tag: flower.name,
              child: FadeInImage(
                image: NetworkImage(flower.photoUrl),
                fit: BoxFit.cover,
                placeholder: AssetImage('assets/images/place_holder.jpg'),
              )),
        ),
        title: Text(flower.name),
        subtitle: Text(flower.description.substring(0, 53) + '...'),
        trailing: CircleAvatar(
          child: Text(
            flower.price.toStringAsFixed(2),
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          radius: 30,
          backgroundColor: Theme.of(context).accentColor,
        ),
      ),
    );
  }
}
